defmodule Dfa do
  def ex1 do
    { 0, [3],
    [
      {0, ?a, 1},
      {0, ?b, 0},
      {1, ?a, 1},
      {1, ?b, 2},
      {2, ?a, 1},
      {2, ?b, 3},
      {3, ?a, 1},
      {3, ?b, 0}
    ]
    }
  end

  def simulate(_delta, _f, _state, _input) do
    false
  end
end
